import time

import redis
from rq import Queue
from flask import Flask

from some_module import count_words_at_url


app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


@app.route('/')
def hello():
    count = get_hit_count()
    q = Queue(connection=redis.Redis(host='redis', port=6379))
    results = [q.enqueue(count_words_at_url, "http://nvie.com") for _ in range(10)]

    time.sleep(4)
    stuff = [r.result for r in results]
    return 'Hello ASDF! Hits: {}\n{}'.format(count, stuff)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
